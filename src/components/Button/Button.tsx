import { ButtonElement } from './style';

export interface ButtonProps {
  type: 'submit' | 'button' | 'reset',
  size?: 'large' | 'small',
  state?: 'primary' | 'no-state'
  disabled?: boolean,
  handleClick: () => void;
}

export const Button: React.FC<ButtonProps> = ({
  children,
  handleClick,
  type = 'button',
  size = 'large',
  state = 'primary',
  disabled = false,
}) => {
  return (
    <ButtonElement
      type={type}
      disabled={disabled}
      size={size}
      state={state}
      onClick={() => handleClick()}
    >
      {children}
    </ButtonElement>
  );
};
