import styled from 'styled-components';
import { baseStyle } from '../../styles/base';

interface styleButtonProps {
  size: string;
  state: 'primary' | 'no-state';
  disabled: boolean;
}

export const ButtonElement = styled.button`
  border: 0;
  text-align: center;
  line-height: 45px;
  width: ${(p: styleButtonProps) => p.size === 'large' ? '150px' : '100px'};
  background-color: ${(p: styleButtonProps) => p.state === 'primary' ? baseStyle.primaryColor : 'transparent'};
  color: ${(p: styleButtonProps) => p.state === 'primary' ? baseStyle.terciaryColor : baseStyle.primaryColor};
  opacity: ${props => props.disabled ? '0.5' : '1'}
`;