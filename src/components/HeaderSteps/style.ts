import styled from 'styled-components';
import { baseStyle } from '../../styles/base';

interface Step {
  active: boolean;
}

export const List = styled.li`
  margin: 0 10px;
  border-radius: 50%;
  text-align: center;
  cursor: pointer;
  background-color: ${baseStyle.primaryColor};
  color: ${baseStyle.terciaryColor};
  height: ${(p: Step) => p.active ? '30px' : '28px'};
  line-height: ${(p: Step) => p.active ? '30px' : '28px'};
  width: ${(p: Step) => p.active ? '30px' : '28px'};
  opacity: ${(p: Step) => p.active ? '1' : '0.5'}
`;

export const WrapList = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #ccd5da;
  height: 70px;
`;

export const EmComponent = styled.em`
  width: 28px;
  line-height: 28px;
  height: 28px;
  text-align: center;
  display: block;
  border-radius: 50%;
  background-color: ${baseStyle.secondaryColor};
`