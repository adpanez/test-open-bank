import {WrapList, List, EmComponent} from './style';

export interface Step {
  id: string;
  active: boolean;
  valid: boolean;
}

export interface HeaderStepsProps {
  steps: Step[];
  handleClick?: () => void;
}

export const HeaderSteps: React.FC<HeaderStepsProps> = ({
  steps,
  handleClick
}) => {
  return (
    <WrapList>
    {
      steps.map((step, index) => {
        return (
          <List key={step.id} active={step.active} onClick={handleClick}>
            {step.valid ? <EmComponent>&#10003;</EmComponent> : index + 1}
          </List>
        )
      })
    }
  </WrapList>
  )
}