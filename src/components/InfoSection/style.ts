import styled from 'styled-components';
import { baseStyle } from '../../styles/base';

export const Title = styled.h3`
  font-size: 16px;
  font-weight: bold;
  margin: 12px 0 7px;
  color: ${baseStyle.primaryColor};
`

export const Description = styled.p`
  margin: 0;
  padding: 0;
  font-size: 14px;
  color: ${baseStyle.primaryColor};
`