import { Title, Description } from './style';

export interface InfoSectionProps {
  title: string;
  description: string;
}

export const InfoSection: React.FC<InfoSectionProps> = ({
  title,
  description
}) => {
  return (
    <div>
      <Title>{title}</Title>
      <Description>{description}</Description>
    </div>
  )
}