import { useDispatch, useSelector } from "react-redux";
import {useIntl} from 'react-intl';
import { Button } from "../Button/Button";
import { initData } from "../../store/interface";
import { WrapControls } from "./style";

export interface ControlsWizzardProps {
  handleClick: () => void;
}

const ControlsWizzard: React.FC<ControlsWizzardProps> = ({
  handleClick
}) => {
  const { terms, currentStep } = useSelector((state: initData) => state);
  const dispatch = useDispatch();
  const intl = useIntl();

  return (
    <WrapControls currentStep={currentStep}>
      {currentStep > 0 &&
        <Button
          type={'button'}
          disabled={false}
          state="no-state"
          handleClick={() => {
            dispatch(
              {
                type: 'CHANGE_STEP',
                addional: {isValid: true, type: 'prev', newCurrentStep: currentStep - 1}
              }
            )
          }}
        >
          {intl.formatMessage({id: 'back'})}
        </Button>}

      <Button
        type={'button'}
        disabled={!terms}
        state="primary"
        handleClick={() => handleClick()}
      >
        {intl.formatMessage({id: 'next'})}
      </Button>
    </WrapControls>
  )
}

export default ControlsWizzard;