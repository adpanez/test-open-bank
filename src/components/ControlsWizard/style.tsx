import styled from "styled-components";

interface ControlsPropStyle {
  currentStep: number;
}

export const WrapControls = styled.div`
  margin: 20px 0;
  display: flex;
  justify-content: ${(p: ControlsPropStyle) => p.currentStep === 0 ? 'flex-end' : 'space-between'};
`