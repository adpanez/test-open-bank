export interface CounterProp {
  maxCount: number;
  currentText: number;
}

const Counter: React.FC<CounterProp> = ({
  maxCount,
  currentText,
}) => (
  <div>
    <span>{currentText}/{maxCount}</span>
  </div>
)

export default Counter;