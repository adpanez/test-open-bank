import { initialValue } from './store';
import { Step } from './interface';

const CHANGE_STEP = 'CHANGE_STEP';
const CHANGE_TERMS = 'CHANGE_TERMS';
const UPDATE_FORM = 'UPDATE_FORM';
const VALID_FORM = 'VALID_FORM';

const handleSetSteps = (steps: Step[], isValid: boolean, action: string) => {
  const newSteps = [...steps];
  const indexCurrentStep = newSteps.map(step => step.active).indexOf(true);
  const indexNextStep = action === 'next' ? indexCurrentStep + 1 : indexCurrentStep - 1;

  newSteps[indexCurrentStep].active = false;
  newSteps[indexCurrentStep].valid = isValid;
  newSteps[indexNextStep].active = true;

  return newSteps;
}

export const reducer = (state = initialValue, action: any) => {
  switch (action.type) {
    case CHANGE_STEP:
      return {
        ...state,
        steps: handleSetSteps(state.steps, action.addional.isValid, action.addional.type),
        currentStep: action.addional.newCurrentStep
      }
    case CHANGE_TERMS:
      return {
        ...state,
        terms: action.value
      }
    case UPDATE_FORM:
      return {
        ...state,
        password: action.form.password,
        password_second: action.form.password_second,
        track: action.form.track
      }
    case VALID_FORM:
      return {
        ...state,
        status_form: action.status_form
      }
    default:
      return {...state}
  }
}