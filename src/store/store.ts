import { createStore } from 'redux';
import { initData } from './interface';
import { reducer } from './reducer';

export const initialValue: initData = {
  steps: [
    {
      id: 'index1',
      active: true,
      valid: false,
    },
    {
      id: 'index2',
      active: false,
      valid: false,
    },
    {
      id: 'index3',
      active: false,
      valid: false,
    }
  ],
  terms: false,
  password: '',
  passwordSecond: '',
  track: '',
  statusForm: 'no-checked',
  currentStep: 0
}

export default createStore(reducer, initialValue);