export interface Step {
  id: string;
  active: boolean;
  valid: boolean;
}

export interface initData {
  steps: Step[],
  terms: boolean,
  password: string,
  passwordSecond: string;
  track: string,
  statusForm: 200 | 401 | 'no-checked',
  currentStep: number;
}
