export const baseStyle = {
  primaryColor: '#002B45',
  secondaryColor: '#FF0049',
  terciaryColor: '#FFF1E5',
  white: '#FFFFFF',
  black: '#000000',
};