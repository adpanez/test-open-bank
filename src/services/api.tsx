const PRUEBA_KO = 'pruebaKO123';

const RESPONSE_OK = {status: 200};
const RESPONSE_KO = {status: 401};

interface ParamSubmit{
  pass: string;
  repass: string;
  optionalQuestion: string;
}

const submitForm = ({pass, repass, optionalQuestion}: ParamSubmit) =>
	new Promise((resolve, reject) =>
		setTimeout(() =>
			pass !== PRUEBA_KO
			? resolve(RESPONSE_OK)
			: reject(RESPONSE_KO)
		, 3000)
)

export {
	submitForm
}