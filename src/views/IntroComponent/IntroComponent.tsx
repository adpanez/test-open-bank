import { useDispatch, useSelector } from "react-redux";
import {useIntl} from 'react-intl'
import ControlsWizzard from "../../components/ControlsWizard/ControlsWizzard";
import { InfoSection } from "../../components/InfoSection/InfoSection";
import { initData } from "../../store/interface";
import { CheckTerms } from "./style";

const IntroComponent: React.FC = () => {
  const { terms, currentStep } = useSelector((state: initData) => state);
  const dispatch = useDispatch();
  const intl = useIntl();
  const literals = (id: string) => intl.formatMessage({id})

  const nextStep = () => {
    dispatch(
      {
        type: 'CHANGE_STEP',
        addional: {isValid: true, type: 'next', newCurrentStep: currentStep + 1}
      }
    )
  }

  return (
    <div>
      <InfoSection
        title={literals("intro-title-1")}
        description={literals("intro-description-1")}
      />
      <InfoSection
        title={literals("intro-title-2")}
        description={literals("intro-description-2")}
      />
      <CheckTerms>
        <input
          id="verify"
          name="verify"
          type="checkbox"
          defaultChecked={terms}
          onChange={(e) => dispatch({type: 'CHANGE_TERMS', value: e.target.checked})}
        />
        <label htmlFor="verify">{literals("terms")}</label>
      </CheckTerms>
      <ControlsWizzard handleClick={nextStep}/>
    </div>
  )
}

export default IntroComponent;