export interface Step {
  id: string;
  valid: boolean;
  active: boolean;
}