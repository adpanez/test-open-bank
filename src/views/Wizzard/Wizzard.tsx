import { useSelector } from 'react-redux'
import {FormattedMessage} from 'react-intl'
import { HeaderSteps } from '../../components/HeaderSteps/HeaderSteps';
import ConfirmComponent from '../ConfirmComponent/ConfirmComponent';
import IntroComponent from '../IntroComponent/IntroComponent';
import FormComponent from '../FormComponent/FormComponent';
import { WrapWizzard, WrapSteps, Title } from './style';
import { initData } from '../../store/interface';


const WizzardComponent: React.FC = () => {
  const { steps, currentStep } = useSelector((state: initData) => state);

  const listStepsComponent = [<IntroComponent />, <FormComponent />, <ConfirmComponent />];

  return (
    <WrapWizzard>
      <HeaderSteps steps={steps} />
      <WrapSteps>
        <Title>
          <FormattedMessage id="title"/>
        </Title>
        {listStepsComponent[currentStep] || steps[0]}
      </WrapSteps>
    </WrapWizzard>
  )
}

export default WizzardComponent;