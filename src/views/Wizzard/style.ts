import styled from "styled-components";
import { baseStyle } from "../../styles/base";

export const WrapWizzard = styled.div`
  width: 900px;
  margin: 0 auto;
`;

export const WrapSteps = styled.div`
  padding: 30px;
  position: relative;
`

export const Title = styled.h3`
  font-size: 20px;
  color: ${baseStyle.primaryColor};
  font-weight: bold;
`