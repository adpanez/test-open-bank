import { useDispatch, useSelector } from "react-redux";
import {FormattedMessage} from 'react-intl';
import { useForm } from "react-hook-form";
import { useState } from "react";
import ControlsWizzard from "../../components/ControlsWizard/ControlsWizzard";
import Counter from '../../components/Counter/Counter';
import { Field, Text, WrapControls, Control, WrapControlTrack, WrapError, Loading } from "./style";
import { initData } from "../../store/interface";
import { submitForm } from "../../services/api";

const errorMessages: any = {
  password: {
    required: <FormattedMessage id="required" />,
    maxLength: <FormattedMessage id="maxlength" />,
    minLength: <FormattedMessage id="minlength" />,
    pattern: <FormattedMessage id="valid-format-password" />,
  },
  password_second: {
    required: <FormattedMessage id="required" />,
    maxLength: <FormattedMessage id="maxlength" />,
    minLength: <FormattedMessage id="minlength" />,
    validate: <FormattedMessage id="valid-equal-passwords" />,
  }
}

const FormComponent: React.FC = () => {
  const { password, passwordSecond, track, currentStep } = useSelector((state: initData) => state);

  const { register, handleSubmit, getValues, formState: { errors } } = useForm();
  const [trackValue, setTrackValue] = useState('');
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const setFormStatus = (res: any) => {
    dispatch(
      {
        type: 'VALID_FORM',
        status_form: res.status
      }
    )
  }

  const onSubmit = async(data: any) => {
    dispatch({type: 'UPDATE_FORM', form: {...data}})
    setLoading(true);
    await(
      submitForm(data).then((res) => {
        setFormStatus(res);
      }).catch((err) => {
        setFormStatus(err);
      })
    )
    dispatch(
      {
        type: 'CHANGE_STEP',
        addional: {isValid: true, type: 'next', newCurrentStep: currentStep + 1}
      }
    )
  };

  const confirmPassword = (value: any) => value === getValues("password");

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {loading && <Loading><FormattedMessage id="loading"/></Loading>}
      <WrapControls>
        <Control>
          <Text htmlFor="password"><FormattedMessage id="password-label" /></Text>
          <Field
            defaultValue={password}
            type="password"
            id="password"
            onInput={() => {}}
            {...register("password", { required: true, maxLength: 24, minLength: 8, pattern: /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/ })}
          />
          {errors.password &&
            <WrapError>{errorMessages['password'][errors.password.type]}</WrapError>}
        </Control>
        <Control>
          <Text htmlFor="password_second"><FormattedMessage id="password-label-repeat" /></Text>
          <Field
            defaultValue={passwordSecond}
            type="password"
            id="password_second"
            {...register("password_second", { required: true, maxLength: 24, minLength: 8, validate: (e) => confirmPassword(e) })}
            onInput={() => {}}
          />
          {errors.password_second &&
            <WrapError>{errorMessages['password_second'][errors.password_second.type]}</WrapError>}
        </Control>
      </WrapControls>
      <WrapControlTrack>
        <Text htmlFor="track"><FormattedMessage id="track-label" /></Text>
        <Field
          defaultValue={track}
          type="text"
          onInput={(e: React.FormEvent<HTMLInputElement>) => setTrackValue(e.currentTarget.value)}
          id="track"
          maxLength={255}
          {...register("track")}
        />
        <Counter maxCount={255} currentText={trackValue.length} />
      </WrapControlTrack>

      <ControlsWizzard handleClick={handleSubmit(onSubmit)}/>
    </form>
  )
}

export default FormComponent;