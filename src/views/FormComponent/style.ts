import styled from "styled-components";
import { baseStyle } from "../../styles/base";

export const Field = styled.input`
  border: 1px solid ${baseStyle.primaryColor};
  height: 35px;
  width: 100%;
`

export const Text = styled.label`
  display: block;
  color: ${baseStyle.primaryColor};
`

export const WrapControls = styled.div`
  display: flex;
`

export const Control = styled.div`
  width: 50%;
  padding: 0 12px;
`

export const WrapControlTrack = styled.div`
  padding: 0 12px;
  margin: 20px 0 0;
`

export const WrapError = styled.em`
  color: ${baseStyle.secondaryColor};
  font-size: 12px;
  display: block;
`

export const Loading = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  background: ${baseStyle.primaryColor};
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${baseStyle.white};
`;
