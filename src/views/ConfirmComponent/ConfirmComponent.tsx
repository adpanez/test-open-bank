import { useSelector } from "react-redux";
import {useIntl} from 'react-intl';
import ControlsWizzard from "../../components/ControlsWizard/ControlsWizzard";
import { InfoSection } from "../../components/InfoSection/InfoSection";
import { initData } from "../../store/interface";
import { WrapperConfirm } from "./style";

const ConfirmComponent: React.FC = () => {
  const status_form = useSelector((state: initData) => state.statusForm);
  const intl = useIntl();

  const collectionMessages: any = {
    200: <InfoSection
            title={intl.formatMessage({id: 'password-created'})}
            description={intl.formatMessage({id: 'password-created-description'})}
          />,
    401: <InfoSection
            title={intl.formatMessage({id: 'password-error'})}
            description={intl.formatMessage({id: 'password-error-description'})}
          />
  }

  const message = collectionMessages[status_form] ||
                    <InfoSection
                      title={intl.formatMessage({id: 'password-no-completed'})}
                      description={intl.formatMessage({id: 'password-no-completed-description'})}
                    />;

  return (
    <WrapperConfirm>
      {message}
      <ControlsWizzard handleClick={() => {alert('Fin del proceso')}}/>
    </WrapperConfirm>
  )
}

export default ConfirmComponent;