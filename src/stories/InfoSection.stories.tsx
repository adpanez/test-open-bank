import { Story, Meta } from '@storybook/react';
import { InfoSection, InfoSectionProps } from '../components/InfoSection/InfoSection';

export default {
  title: 'Example/InfoSection',
  component: InfoSection,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<InfoSectionProps> = (args) => <InfoSection {...args} />;

export const InfoSectionBasic = Template.bind({});
InfoSectionBasic.args = {
  title: 'Título de sección',
  description: 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.'
}