import { Story, Meta } from '@storybook/react';
import { Button, ButtonProps } from '../components/Button/Button';

export default {
  title: 'Example/Button',
  component: Button,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} >Hola</Button>;

export const ButtonPrimary = Template.bind({});
ButtonPrimary.args = {
  type: 'button',
  state: 'primary',
  handleClick: () => alert('Hola mundo')
}

export const ButtonNoState = Template.bind({});
ButtonNoState.args = {
  type: 'button',
  state: 'no-state',
  handleClick: () => alert('Hola mundo')
}

export const ButtonDisabled = Template.bind({});
ButtonDisabled.args = {
  disabled: true,
  type: 'button',
  handleClick: () => alert('Hola mundo')
};
