import { Story, Meta } from '@storybook/react';
import Counter, { CounterProp } from '../components/Counter/Counter';

export default {
  title: 'Example/Counter',
  component: Counter,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<CounterProp> = (args) => <Counter {...args} />;

export const CounterBase = Template.bind({});
CounterBase.args = {
  currentText: 12,
  maxCount: 20,
}