import { Story, Meta } from '@storybook/react';
import { HeaderSteps, HeaderStepsProps } from '../components/HeaderSteps/HeaderSteps';

export default {
  title: 'Example/HeaderSteps',
  component: HeaderSteps,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<HeaderStepsProps> = (args) => <HeaderSteps {...args} >Hola</HeaderSteps>;

export const HeaderStepsBasic = Template.bind({});
HeaderStepsBasic.args = {
  steps: [
    {
      id: 'index-1',
      active: true,
      valid: false,
    },
    {
      id: 'index-2',
      active: false,
      valid: false,
    },
    {
      id: 'index-3',
      active: false,
      valid: false,
    },
  ],
  handleClick: () => alert('Hola mundo')
}

export const HeaderStepsValid = Template.bind({});
HeaderStepsValid.args = {
  steps: [
    {
      id: 'index-1',
      active: false,
      valid: true,
    },
    {
      id: 'index-2',
      active: true,
      valid: false,
    },
    {
      id: 'index-3',
      active: false,
      valid: false,
    },
  ]
}