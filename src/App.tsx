import WizzardComponent from "./views/Wizzard/Wizzard";
import store from './store/store';
import { Provider } from "react-redux";
import {IntlProvider} from 'react-intl'
import intl from './locale/index';

export default function App() {
  return(
    <Provider store={store}>
      <IntlProvider messages={intl} locale="es" defaultLocale="es">
        <WizzardComponent />
      </IntlProvider>
    </Provider>
  )
}